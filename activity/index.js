/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [

    "Hanamichi Sakuragi",
    "Kaede Rukawa",
    "Takenori Akagi",
    "Hisashi Mitsui",
    "Ryota Myagi",
    "Akira Sendo",
    "Shinichi Maki"
];


let friendsList = [];


function showRegisteredUser(){
    
    console.log("%cRegistered Users","color:orange");
    
    for(let i = 0; i < registeredUsers.length;i++){
        console.log( `${i + 1}. ${registeredUsers[i]} `);
    }

};

showRegisteredUser();



/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/



    function register(newUser) {

        let lowerCaseUsers=[];
        registeredUsers.forEach(function(user){
            lowerCaseUsers.push(user.toLowerCase());
        });
   
        let checkUser = lowerCaseUsers.includes(newUser.toLowerCase());
            if(checkUser){
                alert("Registration failed. "+ newUser +" is already exists!");
            } else {
                registeredUsers.push(newUser);
                alert("Thank you for registering!");
          
            }
    };

 


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/



    function addFriend(friend){
   
    let lowerCaseUsers=[];
    registeredUsers.forEach(function(user){
        lowerCaseUsers.push(user.toLowerCase());
    });
   

     let checkFriend = lowerCaseUsers.includes(friend.toLowerCase());

        if(checkFriend){
           
            friendsList.push(friend);
            alert("You have added " + friend + " as a friend!"); 
 
        } else {
            
             alert("User not found.");

        }
    };





/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

    function displayFriend(){
        
        if(friendsList.length === 0){

             alert("You currently have 0 friends. Add one first.");

        } else {      
  
            for(let i = 0; i < friendsList.length;i++){ 
            console.log( `${i + 1}. ${friendsList[i]} `);
            
            }
        }
    };



    
// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/
    function displayNumberOfFriends(){
        if(friendsList.length === 0){

            alert("You currently have 0 friends.");

        } else {

             alert("You currently have " + friendsList.length+ " friends.");
        }
       
    };





/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/
    
    function deleteFriend(){
   
     if(friendsList.length === 0){

            alert("You currently have 0 friends.");

        } else {
        
            friendsList.pop();
        }
       
    };
    




/*======================================================================================*/

