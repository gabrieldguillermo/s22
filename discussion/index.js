// console.log("hello");


// Array Methods

/*
	Mutator Methods
		-seeks to modify the contents of an array
		- are functions that mutate an array after they are created
*/



let fruits = ["Appler","Orange","Kiwi","Watermelon"];

/*
	push()
	-add an element in the end of an array and returns the arrays length

	Syntax:
		arrayName.push(element);
*/

console.log("Current Fruits Array:");
console.log(fruits);

// adding elements

fruits.push("mango");
console.log(fruits);
let fruitLength = fruits.push("melon");
console.log(fruitLength);
console.log(fruits);

fruits.push("Avocado","Guava");
console.log(fruits);




/*
	pop()
		- removed the last element in our array and returns the removed element

		Syntax:
			arrayName.pop();

*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from the pop method");
console.log(fruits);



/* 
	unshift()
	 -adds one or more elements at the beginning of an array
	 -returns the length of the array


	Syntax:
		arrayName.unshift(elementA);
		arrayName.unshift(elementA,elementB);

*/


fruits.unshift("Lime","Banana");
console.log("Mutated Array from the unshift method");
console.log(fruits);




/*
	shift()
		-removes an element at the beginning of our array and return the removed element

	Syntax:
	 	arrayName.shift();


*/

let removedFruit2 = fruits.shift();
console.log(removedFruit2);
console.log("Mutated array from the shift method");
console.log(fruits);



/*
	splice()
		- allows to simultaneously remove elements from a specified index number and adds an element


	Syntax:
		arrayName.splice(starting,deleteCount,elementsToBeAdded);

*/

let fruitSplice = fruits.splice(1,2 ,"cherry","lyche");
console.log("Mutated array from splice method");
console.log(fruits);
console.log(fruitSplice);

// Using Splice without adding elements

let removedSplice2 = fruits.splice(3,2);
console.log(fruits);
console.log(removedSplice2);




/*
	sort()
		-rearrange the array in a alphanumeric order

	Syntax:
		arrayName.sort()

*/
fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);


let mixedArr = [12, "May", 36, 94, "August", 5, 6.3, "September"];
console.log(mixedArr.sort());



/*
	reverse()
		-reverses the order of the elements in an array

	Syntax:
		arrayName.reverse()
*/
fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);



// for sorting the items in descending order 

fruits.sort().reverse();
console.log(fruits);




	/*MINI ACTIVITY:
	 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console


	 	function registerFruit () {
	 		let doesFruitExist = fruits.includes(fruitName);

	 		if(doesFruitExst) {
	 			alerat(fruitName "is already on our inventory")
	 		} else {
	 			fruits.push(fruitName);
	 			break;
	 			alert("Fruit is now listed in our inventory")
	 		}
	 	}
	 	
	*/


	 	// function registerFruit (fruitName) {
	 	// 	let doesFruitExist = fruits.includes(fruitName);

	 	// 	if(doesFruitExist) {
	 	// 		alert(fruitName + " is already on our inventory")
	 	// 	} else {
	 	// 		fruits.push(fruitName);
	 	// 		alert("Fruit is now listed in our inventory");
	 			
	 	// 	}
	 	// }
	 	
	 	// registerFruit("Pineapple");
	 	// console.log(fruits);


/* MINI ACTIVITY - Array Mutators
	- Create an empty array
	- use the push() method and unshift() method to add elements in your array
	- use the pop() and shift() method to remove elements in your array
	- use sort to organize your array in alphanumeric order
	- at the end the array should contain not less than 6 elements after applying these methods.
	- log the changes in the console every after update

*/


let grade = [];
console.log(grade);
grade.push(98, 89, 81,78,86);
console.log(grade);
grade.unshift(91,85,82,83,92);
console.log(grade);
grade.pop();
console.log(grade);
grade.shift();
console.log(grade);
grade.sort();
console.log(grade);



/*
	Non-Mutator Methods
		- these are function or methods that do not modify or change an array after created
		- thes methods do not manipulate the original array performing various task as returning elements from an array and combining arrays and printing the output
*/

let countries = ["US","PH","CAN","SG","TH","PH","FR","DE"];


/*
 	indexOf()	
 		- returns the index number of the first matching element found in an array. If no match as found, the result will be -1. The search proccess will be done from our first element proceeding to the last element

 	Syntax:
 		arrayName.indexof(searchvalue)
 		arrayName.indexof(searchvalue, fromIndex)

*/
	
let firstIndex = countries.indexOf("PH");
console.log("Result of indexof method: " + firstIndex);
firstIndex = countries.indexOf("PH",4);
console.log("Result of indexof method: " + firstIndex);
firstIndex = countries.indexOf("PH",7);
console.log("Result of indexof method: " + firstIndex);
firstIndex = countries.indexOf("PH",-1);
console.log("Result of indexof method: " + firstIndex);



/*
	lastIndexOf()
		-returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first

	Syntax:
		arrayName.lastIndexof(searchvalue)
 		arrayName.lastIndexof(searchvalue, fromIndex)

*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexof method: " + lastIndex);
lastIndex = countries.lastIndexOf("PH",4);
console.log("Result of lastIndexof method: " + lastIndex);


/*
	slice();
		-portions / slice elements from our array and returns a new array

		Syntax:
 		arrayName.slice(startingIndex)
 		arrayName.slice(startingIndex, endingIndex)


*/

console.log(countries);

let sliceArrA = countries.slice(3);
console.log("Result from slice method:");
console.log(sliceArrA);
console.log(countries);

let sliceArrB = countries.slice(2,5);
console.log("Result from slice method:");
console.log(sliceArrB);
console.log(countries);



let sliceArrC = countries.slice(-3);
console.log("Result from slice method:");
console.log(sliceArrC);
console.log(countries);


/* 
	toString()
		-returns an array as a string seperated by commas
		-is used internally by JS when an array needs to be displayed as text(like in HTML), or when an object / array needs to be used as a string.

	Syntax:
		arrayName.toString();

*/


let  stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);




/*
	concat()
		-combineds two or more array and return the combined result

	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA)


*/

let taskArrayA = ["drink HTML", "eat JAVASCRIPT"];
let taskArrayB = ["inhale CSS", "breath SASS"];
let taskArrayC = ["get GIT","be NODE"];


let tasks= taskArrayA.concat(taskArrayB);
console.log("Result from concat method");
console.log(tasks);


let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);



let combinedTask = taskArrayA.concat("smell express", "throw react");
console.log(combinedTask);


/*
	join()
		-returns an array as a string
		-does not change the original array
		- we can use any separator. The default is comma (,)
	
	Syntax:
	 	arrayName.join(separatorString)

*/

let students = ["Elysha","Gab","Ronel", "Jean"];
console.log(students.join());
console.log(students.join(' '));
console.log(students.join(" - "));




/*
	Iteration Methods
		-are loops designed to perform repitive tasks in an array. used for manipulating array data resultign in complex tasks.

		- normally works with a function supplied as an argument

		- aims to evaluate each element in an array
*/


/*
	forEach()
		-similar to for loop that iterates on each array element

		Syntax:
			arrayName.forEach(function( individualElements){
	stament . . 
			})	
	
*/


allTasks.forEach(function (task) {
	console.log(task);
});


let filtredTasks = [];

allTasks.forEach(function (task) {
	console.log(task);
if(task.length > 10){
	filtredTasks.push(task);
}

});
console.log(allTasks);
console.log("Result of filtredTasks:")
console.log(filtredTasks);


/*
	map()
		-iterates on each element and returns a new array with different value depending on the result of the functions operation.

	Syntax:
	  arrayName.map(function(individualElement){
	statement..
	  )}

*/


let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number){
	console.log(number);
	return number * number
});


console.log("Original Array");
console.log(numbers);
console.log("Result of the map method");
console.log(numberMap);



/*
	every()
		-ckecks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and false if otherwise
	
	Syntax:

	Syntax:
	  arrayName.every(function(individualElement){
	return expression/condition
	  )}


*/

let allValid = numbers.every(function(number){
	return (number < 3);
});

console.log("Result of every method");
console.log(allValid);

/*

	some()
		-checks if at least one element in the array meet the given condition. Returns a true value if at least one of the elements meets the given condtion and false if otherwise

	Syntax:
	  arrayName.some(function(individualElement){
	return expression/condition
	  )}

*/

let someValid = numbers.some(function(number){
	return (number < 3);
});

console.log("Result of every method");
console.log(someValid);



/*
	filter()
		- returns a new array that contains elements wcich meets the given condition. Return an empty array in no elements were found (that satisfied the given condition)

	Syntax:
		arrayName.filter(function(individualElement){
			return expression/ condition
		})

*/

let filteredValid = numbers.filter(function(number) {
	return (number < 5)
})

console.log("Result of filter method:")
console.log(filteredValid)



/*
	includes()
		-checks if the argument passed can be found in an array

		-can be chained after another method. The result of the first method is used on the second method until all chained methods have been resolved


*/

let products = ["mouse", "KEYBOARD", "laptop", "monitor"]

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
});

console.log("Result on includes method");
console.log(filteredProducts);